//service to group utils methods
Module.service('UtilsService', [
	'ConfigService',
	'LogService',
	function (
	Config,
	Log
	) {
	'use strict';
	
	var MODULE_NAME = 'UtilsService';
	
	/**
	* set upperCase on first character
	*/
	this.normalizeName = function(name) {
		if(name) {
			return name.charAt(0).toUpperCase() + name.substring(1,name.length);
		}
		return '';
	};		

}]);

