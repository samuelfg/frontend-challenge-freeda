/*jshint sub:true*/
Module.controller('UserController', [
	'$scope',
	'$rootScope',
	'ConfigService',
	'UtilsService',
	'LogService',
	'UserFactory',
	function(
		$scope,
		$rootScope,
		Config,
		Utils,
		Log,
		User
	){
	'use strict';
	
	var MODULE_NAME = 'UserController';
	
	//visible by view
	$scope.utils = Utils;
	
	$scope.getCustomers = function() {
		User.get().then(function (data) {
			$scope.customers = data.response.results;
		});
	};
	
	$scope.editUser = function(i) {
		$scope.currentCustomerIndex = i;
		Log.object($scope.customers[$scope.currentCustomerIndex]);
		$('#inputPhone').val($scope.customers[$scope.currentCustomerIndex].phone);
		$('#inputEmail').val($scope.customers[$scope.currentCustomerIndex].email);
		$('#editModal').modal();
	};
	
	$scope.saveUser = function() {
		$scope.customers[$scope.currentCustomerIndex].phone = $('#inputPhone').val();
		$scope.customers[$scope.currentCustomerIndex].email = $('#inputEmail').val();
		$('#editModal').modal('hide');
	};
	
	function initialize() {
		$scope.getCustomers();
	}
	
	initialize();
}]);

