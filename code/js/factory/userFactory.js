Module.factory('UserFactory', [
	'$http',
	'UriService',
	'ConfigService',
	'UtilsService',
	'LogService',
	function(
		$http,
		Uri,
		Config,
		Utils,
		Log
	){
	'use strict';
	
	var Factory = {};	
	var MODULE_NAME = 'UserFactory';
	
	Factory.get = function() {
		Log.info('get');
		return _get().then(function (data) {
			Log.object(data);
			return data;
		});
	};	
		
	//*** private methods ****************************************************

	
	function _get() {
		Log.info(Uri.customers);
		return $http.get(Uri.customers)
			.then(function (response) {
				return {
					success: true,
					response: response.data
				};
			})
			.catch(function (error) {		
				throw error.statusText;
			});
	}
		
	return Factory;

}]);