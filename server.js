var StaticServer = require('static-server');
var http = require('http');
var server = new StaticServer({
	rootPath: 'build/',           
	name: 'my-http-server', 
	port: 8585,        
	host: '127.0.0.1',   
	cors: '*',         
	followSymlink: true,  
	templates: {
	index: 'index.htm',   
}
});

server.start(function () {
	console.log('Server listening to', server.port);
	console.log('Access by:');
	console.log('-------------------------------------');
	console.log('http://localhost:8585/index.html');
	console.log('-------------------------------------');
});